param ($path="c:\Program Files\Zenimax Online\TheElderScrollsOnline\The Elder Scrolls Online PTS")

$FilePath = "$path\depot\eso.mnf"
if (-not(Test-Path -Path $FilePath)) {
	Write-Output "Path '$FilePath' not found! Exiting..."
	exit 1;
} else {
	Write-Output "Path '$FilePath' found! Processing..."
}

Write-Output "Extracting..."
$e1 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 1 -E 540000 -y dds --extractsubfile combined . }
$e2 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 540001 -E 610000 -y dds --extractsubfile combined . }
$e3 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 610001 -E 650000 -y dds --extractsubfile combined . }
$e4 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 650001 -E 700000 -y dds --extractsubfile combined . }
$e5 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 700001 -E 750000 -y dds --extractsubfile combined . }
$e6 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 750001 -E 810000 -y dds --extractsubfile combined . }
$e7 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 810001 -E 870000 -y dds --extractsubfile combined . }
$e8 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 870001 -E 910000 -y dds --extractsubfile combined . }
$e9 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 910001 -E 940000 -y dds --extractsubfile combined . }
$e10 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 940001 -E 1400000 -y dds --extractsubfile combined . }
$e11 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 0 -E 100000 -y lang --extractsubfile combined . }
$e12 = Start-Job -ArgumentList $FilePath -ScriptBlock { .\lib\EsoExtractData\EsoExtractData.exe $($args[0]) -S 0 -E 100000 -y lua --extractsubfile combined . }
Get-Job $e1.Name,$e2.Name,$e3.Name,$e4.Name,$e5.Name,$e6.Name,$e7.Name,$e8.Name,$e9.Name,$e10.Name,$e11.Name,$e12.Name
Wait-Job $e1.Name,$e2.Name,$e3.Name,$e4.Name,$e5.Name,$e6.Name,$e7.Name,$e8.Name,$e9.Name,$e10.Name

Write-Output "Deleting unneeded assets..."
rm -r .\art\character\
rm -r .\art\environment\
rm -r .\art\fixture\
rm -r .\art\fx\
rm -r .\art\lighting\
rm -r .\art\ppfx\
rm -r .\art\resources\
rm -r .\art\utility\
rm -r .\esoui\art\actionbar\
rm -r .\esoui\art\collectibles\
rm -r .\esoui\art\crowncrates\
rm -r .\esoui\art\legal\
rm -r .\esoui\art\lfg\
rm -r .\esoui\art\mountportraits\
rm -r .\esoui\art\nameplate\
rm -r .\esoui\art\reticles\
rm -r .\esoui\art\rewards\
rm -r .\esoui\art\store\
rm -r .\esoui\art\tutorial\examples\
rm -r .\esoui\art\icons\alchemy\
rm -r .\esoui\art\icons\banner\
rm -r .\esoui\art\icons\emotes\
rm -r .\esoui\art\icons\verses\
rm -r .\esoui\art\icons\visions\
rm -r .\esoui\art\icons\adorn*
rm -r .\esoui\art\icons\adr_*
rm -r .\esoui\art\icons\assistant_*
rm -r .\esoui\art\icons\bm_*
rm -r .\esoui\art\icons\camelmount_*
rm -r .\esoui\art\icons\catlargemount_*
rm -r .\esoui\art\icons\coldharborlore_*
rm -r .\esoui\art\icons\collectable_*
rm -r .\esoui\art\icons\collectible_*
rm -r .\esoui\art\icons\comp_*
rm -r .\esoui\art\icons\companion_*
rm -r .\esoui\art\icons\companions_*
rm -r .\esoui\art\icons\cos_*
rm -r .\esoui\art\icons\costume_*
rm -r .\esoui\art\icons\crownstore_*
rm -r .\esoui\art\icons\daedricprinceslore_*
rm -r .\esoui\art\icons\divineslore_*
rm -r .\esoui\art\icons\dwemerlore_*
rm -r .\esoui\art\icons\dyestamp_*
rm -r .\esoui\art\icons\emo_*
rm -r .\esoui\art\icons\emote_*
rm -r .\esoui\art\icons\fm_*
rm -r .\esoui\art\icons\furniture_*
rm -r .\esoui\art\icons\hair_*
rm -r .\esoui\art\icons\har_*
rm -r .\esoui\art\icons\hat_*
rm -r .\esoui\art\icons\hirelingmail_*
rm -r .\esoui\art\icons\houseguest_*
rm -r .\esoui\art\icons\housing_]uni_*
rm -r .\esoui\art\icons\housing_a*
rm -r .\esoui\art\icons\housing_b*
rm -r .\esoui\art\icons\housing_c*
rm -r .\esoui\art\icons\housing_d*
rm -r .\esoui\art\icons\housing_e*
rm -r .\esoui\art\icons\housing_f*
rm -r .\esoui\art\icons\housing_g*
rm -r .\esoui\art\icons\housing_h*
rm -r .\esoui\art\icons\housing_i*
rm -r .\esoui\art\icons\housing_j*
rm -r .\esoui\art\icons\housing_k*
rm -r .\esoui\art\icons\housing_l*
rm -r .\esoui\art\icons\housing_m*
rm -r .\esoui\art\icons\housing_n*
rm -r .\esoui\art\icons\housing_o*
rm -r .\esoui\art\icons\housing_p*
rm -r .\esoui\art\icons\housing_r*
rm -r .\esoui\art\icons\housing_s*
rm -r .\esoui\art\icons\housing_tel*
rm -r .\esoui\art\icons\housing_the*
rm -r .\esoui\art\icons\housing_tlv*
rm -r .\esoui\art\icons\housing_torchbug*
rm -r .\esoui\art\icons\housing_tre*
rm -r .\esoui\art\icons\housing_u*
rm -r .\esoui\art\icons\housing_v*
rm -r .\esoui\art\icons\housing_w*
rm -r .\esoui\art\icons\hsg*
rm -r .\esoui\art\icons\indrikberry_*
rm -r .\esoui\art\icons\indrikfeather_*
rm -r .\esoui\art\icons\justice_*
rm -r .\esoui\art\icons\jewelrycrafting_*
rm -r .\esoui\art\icons\lore_*
rm -r .\esoui\art\icons\magickalore_*
rm -r .\esoui\art\icons\marking_*
rm -r .\esoui\art\icons\master_writ_*
rm -r .\esoui\art\icons\memento_*
rm -r .\esoui\art\icons\momento_*
rm -r .\esoui\art\icons\mount*
rm -r .\esoui\art\icons\mounticon_*
rm -r .\esoui\art\icons\mrf_*
rm -r .\esoui\art\icons\mrk_*
rm -r .\esoui\art\icons\personality_*
rm -r .\esoui\art\icons\pet_*
rm -r .\esoui\art\icons\skin_*
rm -r .\esoui\art\icons\tribute_*
rm -r .\esoui\art\icons\u44_memento_*
rm -r .\esoui\art\icons\u44_furniture_*
rm -r .\esoui\art\treeicons\housing_*
rm -r .\esoui\art\treeicons\store_*
rm -r .\esoui\art\tutorial\_help_*

Write-Output "Converting assets..."
$p1 = Start-Job -ScriptBlock {
	mogrify -format avif .\esoui\art\characterwindow\*.dds
	mogrify -format avif .\esoui\art\loadingscreens\*.dds
}
$p2 = Start-Job -ScriptBlock {
	mogrify -format avif .\esoui\art\currency\*.dds
	mogrify -format avif .\esoui\art\cursors\*.dds
	mogrify -format avif .\esoui\art\ava\*.dds
	mogrify -format avif .\esoui\art\champion\*.dds
	mogrify -format avif .\esoui\art\charactercreate\*.dds
	mogrify -format jpg -quality 80 -resize 50% .\esoui\art\loadingscreens\*.dds
}
$p3 = Start-Job -ScriptBlock {
	mogrify -format avif .\esoui\art\treasuremaps\*.dds
}
$p4 = Start-Job -ScriptBlock {
	mogrify -format avif .\esoui\art\tribute\*.dds
	mogrify -format avif .\esoui\art\tutorial\*.dds
	mogrify -format avif .\esoui\art\upsell\*.dds
	mogrify -format avif .\esoui\art\loadingtips\*.dds
	mogrify -format avif .\esoui\art\race\*.dds
	mogrify -format avif .\esoui\art\treeicons\*.dds
	dir -recurse -Directory .\esoui\art\charactercreate | %{mogrify -format avif "$_\*.dds"}
	dir -recurse -Directory .\esoui\art\characterwindow | %{mogrify -format avif "$_\*.dds"}
	dir -recurse -Directory .\esoui\art\currency | %{mogrify -format avif "$_\*.dds"}
	dir -recurse -Directory .\esoui\art\treeicons | %{mogrify -format avif "$_\*.dds"}
	dir -recurse -Directory .\esoui\art\tribute | %{mogrify -format avif "$_\*.dds"}
	dir -recurse -Directory .\esoui\art\tutorial | %{mogrify -format avif "$_\*.dds"}
}
$p5 = Start-Job -ScriptBlock {
	dir -recurse -Directory .\esoui\art | %{rm -r "$_\*.png.dds"}
	mogrify -format png .\esoui\art\icons\quest_key_003.dds
	mogrify -format avif .\esoui\art\icons\class\*.dds
	mogrify -format png .\esoui\art\icons\guildfinderheraldry\*.dds
	mogrify -format avif .\esoui\art\icons\guildfinderheraldry\*.dds
	mogrify -format avif .\esoui\art\icons\guildranks\*.dds
	mogrify -format avif .\esoui\art\icons\internal\*.dds
	mogrify -format avif .\esoui\art\icons\mapkey\*.dds
	mogrify -format avif .\esoui\art\icons\placeholder\*.dds
	mogrify -format avif .\esoui\art\icons\poi\*.dds
	mogrify -format avif .\esoui\art\icons\servicemappins\*.dds
	mogrify -format avif .\esoui\art\icons\servicetooltipicons\*.dds
	mogrify -format avif .\esoui\art\icons\worldeventunits\*.dds
	dir -recurse -Directory .\esoui\art\icons\class | %{mogrify -format avif "$_\*.dds"}
	dir -recurse -Directory .\esoui\art\icons\guildranks | %{mogrify -format avif "$_\*.dds"}
	dir -recurse -Directory .\esoui\art\icons\servicetooltipicons | %{mogrify -format avif "$_\*.dds"}
}
$p6 = Start-Job -ScriptBlock {
	mogrify -format avif .\esoui\art\icons\*.dds
}
$p7 = Start-Job -ScriptBlock {
	dir -recurse -Directory .\art\maps | %{mogrify -format avif "$_\*.dds"}
}
Get-Job $p1.Name,$p2.Name,$p3.Name,$p4.Name,$p5.Name,$p6.Name,$p7.Name
Wait-Job $p1.Name,$p2.Name,$p3.Name,$p4.Name,$p5.Name,$p6.Name,$p7.Name

Write-Output "Final cleanup..."
$r1 = Start-Job -ScriptBlock {
	dir -recurse -Directory .\esoui\art | %{rm -r "$_\*.dds"}
}
$r2 = Start-Job -ScriptBlock {
	dir -recurse -Directory .\art\maps | %{rm -r "$_\*.dds"}
}
Get-Job $r1.Name,$r2.Name
Wait-Job $r1.Name,$r2.Name
